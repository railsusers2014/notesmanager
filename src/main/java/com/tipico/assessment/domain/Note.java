package com.tipico.assessment.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="note")
public class Note implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String text;
	private Date   creationDate;
	
	protected Note(){
		//for the shake of JPA, don't use it directly
		this.id = -1;
		this.text = "";
		this.creationDate = new Date();
	}
	public Note(String text) {
		super();
		this.id = -1;
		this.text = text;
		this.creationDate = new Date();
	}
	public Note(long id, String text, Date creationDate) {
		super();
		this.id = id;
		this.text = text;
		this.creationDate = creationDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public String toString() {
		return String.format("Note[id=%d, text='%s', creationDate='%s']",
                id, text, creationDate.toString());
	}
}