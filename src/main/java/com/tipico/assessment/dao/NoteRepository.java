package com.tipico.assessment.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.tipico.assessment.domain.Note;

@Repository("noteRepository")
public interface NoteRepository extends PagingAndSortingRepository<Note, Long> {
	
}