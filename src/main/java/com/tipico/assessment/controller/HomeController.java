package com.tipico.assessment.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping(value = {"/", "home.htm"}, method = RequestMethod.GET)
	public String viewHome(Locale locale, Model model) {
		return "home";
	}
	
	@RequestMapping(value = {"comments.htm"}, method = RequestMethod.GET)
	public String viewComments(Locale locale, Model model) {
		return "comments";
	}
}
