package com.tipico.assessment.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.tipico.assessment.dao.NoteRepository;
import com.tipico.assessment.domain.Note;

@Service("noteService")
public class NoteServiceImpl implements NoteService {

	private final static Logger LOG = LoggerFactory.getLogger(NoteService.class);
	   	
	@Autowired
	private NoteRepository noteRepository;

	@Override
	public Note createNote(Note note) {
		
		LOG.debug("Creating  a new note with text '{}'" , note.getText());
		Note newNote = this.noteRepository.save(note);
		
		LOG.debug("Created the new note {}", newNote);
		return newNote;
	}

	@Override
	public List<Note> getAllNotes() {

		LOG.debug("Getting all notes");
		
		List<Note>	results = new ArrayList<Note>();
		for(Note note: this.noteRepository.findAll())
			results.add(note);
		
		LOG.debug("Found {} notes to return ", results.size());
		return results;
	}

	@Override
	public Page<Note> getPageOfNotes(int pageNumber, int pageSize, String sortField, Direction sortOrder) {
		 
		LOG.debug("Getting a page with notes (pageNumber={}, pageSize={}", pageNumber, pageSize);
		return this.noteRepository.findAll( new PageRequest(pageNumber, pageSize, new Sort(sortOrder, sortField)) );
	}

}